# C-Beam OX Plates

These plates fit a C-Beam gantry instead of the standard doubled
2060, and have holes to fit either a 2060 spindle bed or a C-Beam
spindle bed. They also have holes to thread M2.5 on 9.5mm centers
to support microswitches for homing in any direction to allow
work in any quadrant, and other holes to thread M2.5 for mounting
terminal blocks.

They are based on the Big OX plates, but with consistent hole sizes
and location, and the back plate is not hollowed. The Big OX plates
were built in Sketchup and had somewhat approximate hole sizes
and locations.  The terminal block and limit switch mounting hole
locations are derived from the [SMW3D/Hobby-Fab](https://smw3d-ox.makerforums.info/)
variant of the standard OX plates.

The SuckitDustBoot directory contains a set of replacement Suckit
Dust Boot rails. If you have the original OX Sucket dust boot, the
rails will not fit this base, because it is held on by integrating
wheel axis screws. These rails are replacements for the original
rails that allow the arms and boot to adapt, if you are using the
2060 spindle bed variant.

* I recommend printing in PETG for dimensional stability and toughness.
  If you use a different plastic, you will probably have to scale for
  shrinkage.  I used 7 top and bottom layers, 8 perimeters, and 70%
  infill.  I printed without support. This required minor cleanup in
  the axle screw head recesses for fit.

* You will have to replace the screws that hold the arm retaining
  plates to the bases with screws that are at least 2mm shorter, or
  shorten the existing screws, because the existing screws interfere
  with the axle screw heads.  I have sized this to replace the
  #4 imperial screws shipped with the kit with the similar-sized
  M3x16 screw. These fit the existing washers.

* Tap the bottom and top arm retaining plate holes M3 but do not
  tap them all the way through. The untapped part will act like
  a lock nut and prevent the screw from walking out.

* The chamfered holes are for M3 flat-head screws to hold the
  rails to the plate. The plates are not designed with corresponding
  holes as cut. Transfer the hole locations to the plate and drill
  3.2mm and tap M3.  Secure the M3 screws to the plate with thread
  lock compound.  The bottom two screws are required; the top two
  screws may be omitted if the larger holes are used instead.

* The large holes at the top may optionally replace the top M3
  holes. You may drill then out to 5mm and use an M5 screw to an
  M5 locknut on the back side of the plate, or tap them M5 partway
  through from the back and use an ultra-low profile screw through
  the plate into the rail to lock them in place. If you do that,
  do it before installing the plate to the gantry.

* The design is made in FreeCAD using the assembly3 branch by
  realthunder.  This uses its ability to check in a tree as text
  files optimized for version control.  Until upstream FreeCAD
  takes that feature, upstream FreeCAD will be unable to read
  this design.

## Credits

* [Mark Carew's OpenBuilds OX](https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/)
  The original OX.
  
* [Metalguru](https://openbuilds.com/builds/big-ox-heavy-duty.4040/)
  3D Tech Big Ox HD17 plates

* [SMW3D/Hobby-Fab OX](https://smw3d-ox.makerforums.info/)

* Erik Lien converted the sketchup files to SolidWorks and did a lot
  of the heavy lifting of improving the sketches in SolidWorks, before
  I did additional regularization and added holes. He was also the
  source of the terminal block mounting on the SMW3D/Hobby-Fab OX.
